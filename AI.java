public class AI {
	
	
	// board is scored by 
	public static int scoreBoardCPU(Board b) {
		int[] dists = DistanceTable.shortestRoute(b);
		return (b.cpu.wallsLeft*2 + dists[0]) - (b.p.wallsLeft*2 + dists[1]);
	}
	
	
	
	
}