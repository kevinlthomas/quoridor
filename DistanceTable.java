import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JFrame;


public class DistanceTable {
	
	Integer[][] dist;
	
	// p then cpu
	public static int[] shortestRoute(Board b) {
		int[] vals = new int[2];
		DistanceTable dt = new DistanceTable(b.p.x, b.p.y, b);
		int dist = 10000000;
		for (int i = 0; i < 9; i++) {
			if (dt.dist[i][0] != null)
				dist = Math.min(dt.dist[i][0], dist);
		}
		vals[0] = dist;
		dt = new DistanceTable(b.cpu.x, b.cpu.y, b);
		dist = 10000000;
		for (int i = 0; i < 9; i++) {
			if (dt.dist[i][8] != null)
				dist = Math.min(dt.dist[i][8], dist);
		}
		vals[1] = dist;
		return vals;
	}
	
	public DistanceTable(int x, int y, Board b) {
		dist = new Integer[9][9];
		ArrayList<Point> latest = new ArrayList<Point>();
		latest.add(new Point(x, y));
		dist[x][y] = 0;
		updateTable(b, latest, 1);
	}
	
	public void updateTable(Board b, ArrayList<Point> latest, int i) {
		if (latest.size() == 0) return;
		ArrayList<Point> newlatest = new ArrayList<Point>();
		for (Point p : latest) {
			if (b.board[p.x][p.y].north != null) {
				if (dist[p.x][p.y-1] == null) {
					dist[p.x][p.y-1] = i;
					newlatest.add(0,new Point(p.x,p.y - 1));
				}
			}
			if (b.board[p.x][p.y].south != null) {
				if (dist[p.x][p.y+1] == null) {
					dist[p.x][p.y+1] = i;
					newlatest.add(0,new Point(p.x,p.y+1));
				}
			}
			if (b.board[p.x][p.y].east != null) {
				if (dist[p.x+1][p.y] == null) {
					dist[p.x+1][p.y] = i;
					newlatest.add(0,new Point(p.x+1,p.y));
				}
			}
			if (b.board[p.x][p.y].west != null) {
				if (dist[p.x-1][p.y] == null) {
					dist[p.x-1][p.y] = i;
					newlatest.add(0,new Point(p.x-1,p.y));
				}
			}
		}
		updateTable(b, newlatest, i+1);
	}
	
	public String toString() {
		String acc = "";
		for (int y = 0; y < 9; y++) {
			for (int x = 0; x < 9; x++) {
				acc += dist[x][y] + " ";
			}
			acc += "\n";
		}
		return acc;
	}
	
	public static void main(String[] args) {
		//JFrame f = new JFrame("test");
		//f.setBounds(100,100,600,600);
		//f.setVisible(true);
		//f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Board board = new Board();
		//board.addWall(new Wall(0,0,0,1),"00h");
		//board.addWall(new Wall(1,0,1,1),"00h");
		//board.addWall(new Wall(2,0,2,1),"20h");
		//board.addWall(new Wall(3,0,3,1),"20h");
		//board.addWall(new Wall(4,0,4,1),"40h");
		//board.addWall(new Wall(5,0,5,1),"40h");
		//f.add(board);
		//System.out.println(new DistanceTable(4, 0, board));
		

		PlayGame game = new PlayGame(false, 2);
		game.exacute("00h");
		game.exacute("20h");
		game.exacute("40h");
		System.out.println("TESTTTT: " + game.isValidBoard());
		System.out.println(new DistanceTable(4, 8, game.board));
		
	}
}