import java.awt.Point;
import java.util.ArrayList;

public class DirectedGraph {
	
	Node[][] dg;
	public class Node {
		int x, y;
		Integer dist;
		Node north, south, east, west;
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
	}
	
	public DirectedGraph(int x, int y, Board b) {
		dg = new Node[9][9];
		dg[x][y] = new Node(x, y);
		ArrayList<Point> latest = new ArrayList<Point>();
		latest.add(new Point(x, y));
		fillDG(b, latest);
	}

	private void fillDG(Board b, ArrayList<Point> latest) {
		if (latest.size() == 0) return;
		ArrayList<Point> newlatest = new ArrayList<Point>();
		for (Point p : latest) {
			if (b.board[p.x][p.y].north != null) {
				if (dg[p.x][p.y-1] == null) {
					dg[p.x][p.y-1] = new Node(p.x,p.y-1);
					newlatest.add(new Point(p.x,p.y - 1));
				}
				if (dg[p.x][p.y-1].south != dg[p.x][p.y])
				dg[p.x][p.y].north = dg[p.x][p.y-1];
			}
			if (b.board[p.x][p.y].south != null) {
				if (dg[p.x][p.y+1] == null) {
					dg[p.x][p.y+1] = new Node(p.x,p.y+1);
					newlatest.add(new Point(p.x,p.y+1));
				}
				if (dg[p.x][p.y+1].north != dg[p.x][p.y])
				dg[p.x][p.y].south = dg[p.x][p.y+1];
			}
			if (b.board[p.x][p.y].east != null) {
				if (dg[p.x+1][p.y] == null) {
					dg[p.x+1][p.y] = new Node(p.x+1,p.y);
					newlatest.add(new Point(p.x+1,p.y));
				}
				if (dg[p.x+1][p.y].west != dg[p.x][p.y])
				dg[p.x][p.y].east = dg[p.x+1][p.y];
			}
			if (b.board[p.x][p.y].west != null) {
				if (dg[p.x-1][p.y] == null) {
					dg[p.x-1][p.y] = new Node(p.x-1,p.y);
					newlatest.add(new Point(p.x-1,p.y));
				}
				if (dg[p.x-1][p.y].east != dg[p.x][p.y])
				dg[p.x][p.y].west = dg[p.x-1][p.y];
			}
		}
		fillDG(b, newlatest);
	}
	
	
	public void setInitDist() {
		System.out.println(dg.length);
		for (int i = 2; i < 9; i++)
			dg[i][0].dist = 0;
	}
	public void updateDist(Node n) {
		if (n.north != null) {
			if (n.north.dist == null)
				updateDist(n.north);
			if (n.north.dist != null) {
				if (n.dist == null)
					n.dist = n.north.dist + 1;
				else if (n.dist > n.north.dist)
					n.dist = n.north.dist + 1;
			}
		}
		if (n.west != null) {
			if (n.west.dist == null)
				updateDist(n.west);
			if (n.west.dist != null) {
				if (n.dist == null)
					n.dist = n.west.dist + 1;
				else if (n.dist > n.west.dist)
					n.dist = n.west.dist + 1;
			}
		}
		if (n.south != null) {
			if (n.south.dist == null)
				updateDist(n.south);
			if (n.south.dist != null) {
				if (n.south == null)
					n.dist = n.south.dist + 1;
				else if (n.dist > n.south.dist)
					n.dist = n.south.dist + 1;
			}
			if (n.east != null) {
				if (n.east.dist == null)
					updateDist(n.east);
				if (n.east.dist != null) {
					if (n.dist == null)
						n.dist = n.east.dist + 1;
					else if (n.dist > n.east.dist)
						n.dist = n.east.dist + 1;
				}
			}
		}
	}
	
	public String toString() {
		String acc = "";
		for (int y = 0; y < 9; y++) {
			for (int x = 0; x < 9; x++) {
				if (dg[x][y] == null) acc += "X   |";
				else { 
					if (dg[x][y].north != null) acc += "N"; else acc += " ";
					if (dg[x][y].south != null) acc += "S"; else acc += " ";
					if (dg[x][y].east != null) acc += "E"; else acc += " ";
					if (dg[x][y].west != null) acc += "W|"; else acc += " |";
				}
				acc += " ";
			}
			acc += "\n";
		}
		return acc;
	}
	
	public ArrayList<String> stringify(Node n) {
		ArrayList<String> acc = new ArrayList<String>();
		if (n.north != null) {
			ArrayList<String> north = stringify(n.north);
			for (String str : north)
				acc.add("("+n.x+","+n.y+")"+str);
		}
		if (n.south != null) {
			ArrayList<String> south = stringify(n.south);
			for (String str : south)
				acc.add("("+n.x+","+n.y+")"+str);
		}

		if (n.west != null) {
			ArrayList<String> west = stringify(n.west);
			for (String str : west)
				acc.add("("+n.x+","+n.y+")"+str);
		}
		if (n.east != null) {
			ArrayList<String> east = stringify(n.east);
			for (String str : east)
				acc.add("("+n.x+","+n.y+")"+str);
		}
		if (n.east == null && n.west == null && n.south == null && n.north == null)
			acc.add("("+n.x+","+n.y+")");
		return acc;
	}
	
	public ArrayList<ArrayList<Point>> converToSequences(Node n) {
		ArrayList<ArrayList<Point>> acc = new ArrayList<ArrayList<Point>>();
		if (n.north != null) {
			ArrayList<ArrayList<Point>> north = converToSequences(n.north);
			for (ArrayList<Point> str : north) {
				str.add(0,new Point(n.x,n.y));
				acc.add(str);
			}
		}
		if (n.south != null) {
			ArrayList<ArrayList<Point>> south = converToSequences(n.south);
			for (ArrayList<Point> str : south) {
				str.add(0,new Point(n.x,n.y));
				acc.add(str);
			}
		}

		if (n.west != null) {
			ArrayList<ArrayList<Point>> west = converToSequences(n.west);
			for (ArrayList<Point> str : west) {
				str.add(0,new Point(n.x,n.y));
				acc.add(str);
			}
		}
		if (n.east != null) {
			ArrayList<ArrayList<Point>> east = converToSequences(n.east);
			for (ArrayList<Point> str : east) {
				str.add(0,new Point(n.x,n.y));
				acc.add(str);
			}
		}
		if (n.east == null && n.west == null && n.south == null && n.north == null) {
			ArrayList<Point> arrp = new ArrayList<Point>();
			arrp.add(new Point(n.x,n.y));
			acc.add(arrp);
		}
		return acc;
	}
	
}