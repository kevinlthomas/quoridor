import java.awt.Point;
import java.util.ArrayList;

public class SearchTree {
		private boolean validPath;
		public int x, y, goal, dist;
		private SearchTree[] next;
		private ArrayList<Point> visited;
		

	public SearchTree(int x, int y, ArrayList<Point> v, int goal, Board b) {
		//System.out.println("SEARCHTREE: "+x+","+y+","+goal+"  |  "+v);
		this.x = x;
		this.y = y;
		next = new SearchTree[4];
		this.goal = goal;
			if (contains(v,x,y)) {
				validPath = false;
				dist = 1000000;
				return;
			}
			visited = copy(v);
			visited.add(new Point(x,y));
		if (y == goal) {
			validPath = true;
			dist = 0;
		} else {
			dist = 10000;
			System.out.println(y);
			if (y-1 >= 0 && b.board[x][y].north != null) {
				SearchTree north = new SearchTree(x, y - 1, visited, goal, b);
				if (north.validPath) {
					next[0] = north;
					this.validPath = true;
					if (dist > north.dist)
					dist = 1 + north.dist;
				}
					
			}
			if (y+1 < 9 && b.board[x][y].south != null) {
				SearchTree south = new SearchTree(x, y + 1, visited, goal, b);
				if (south.validPath) {
					next[1] = south;
					this.validPath = true;
					if (dist > south.dist)
						dist = 1 + south.dist;
				}
			}
			if (x+1 < 9 && b.board[x][y].east != null) {
				SearchTree east = new SearchTree(x+1, y, visited, goal, b);
				if (east.validPath) {
					next[2] = east;
					this.validPath = true;
					if (dist > east.dist)
						dist = 1 + east.dist;
				}
			}
			if (x-1 >= 0 && b.board[x][y].west != null) {
				SearchTree west = new SearchTree(x+1, y, visited, goal, b);
				if (west.validPath) {
					next[3] = west;
					this.validPath = true;
					if (dist > west.dist)
						dist = 1 + west.dist;
				}
			}
		}
	}
	
	
	
	public static int[][] generateDistGrid(int x, int y, int goal) {
		int[][] dists = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				dists[i][j] = 100000;
		return gDG(new SearchTree(x, y, new ArrayList<Point>(), goal), dists);
	}
	
	public static int[][] gDG(SearchTree st, int[][] dists) {
		if (st == null) return dists;
		if (dists[st.x][st.y] > st.dist)
			dists[st.x][st.y] = st.dist;
	
		return gDG(st.next[0],gDG(st.next[1],gDG(st.next[2],gDG(st.next[3],dists))));
	}
	
	
	public ArrayList<Point> copy(ArrayList<Point> og) {
		ArrayList<Point> newarr = new ArrayList<Point>();
		for (Point p : og) newarr.add(p);
		return newarr;
	}
	
	public boolean contains(ArrayList<Point> v, int x, int y) {
		for (Point p : v) {
			if (p.x == x && p.y == y) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return "("+x+","+y+")  DIST: "+dist+"  ["+next[0]+" , "+next[1]+" , "+next[2]+" , "+next[3]+"]";
	}
	
	public void printTree(int depth) {
		if (this == null) return;
		String acc = "";
		for (int i = 0; i < depth; i++) acc += "  ";
		System.out.println(acc+"("+x+","+y+")  DIST: "+dist);
		if (next[0] != null)
		next[0].printTree(depth+1);
		if (next[1] != null)
		next[1].printTree(depth+1);
		if (next[2] != null)
		next[2].printTree(depth+1);
		if (next[3] != null)
		next[3].printTree(depth+1);
	}
	
	
	public static void main(String[] args) {
		SearchTree st = new SearchTree(5, 7, new ArrayList<Point>(), 0);
		//System.out.println(st);
		//System.out.println(st.next[1]);
		st.printTree(0);
		System.out.println(st.dist);
		int[][] dists = generateDistGrid(5, 0, 0);
		for (int[] arr : dists) {
			for (int i : arr)
				System.out.print(i+" ");
			System.out.println("");
		}
	}
	




}