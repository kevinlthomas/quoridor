import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;


public class HighScores {
	

	public static ArrayList<Score> loadHS() {
		ArrayList<Score> scores = new ArrayList<Score>();
	  	String name;
	  	String nextLine;
	  	int score;
		try {
	        FileInputStream is = new FileInputStream("HighScores.txt");
	        Reader isr = new InputStreamReader(is);
		  	BufferedReader bf = new BufferedReader(isr);
			nextLine = bf.readLine();

		  	while (nextLine != null) {
		  		name = nextLine;
		  		nextLine = bf.readLine();
		  		if (nextLine == null) {
		  			break;
		  		} else {
		  			score = Integer.parseInt(nextLine);
		  			nextLine = bf.readLine();
		  			scores.add(new Score(name, score));
		  		}
		  	}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scores;
	}	
	
	
	public static void addHS(Score score) {
		ArrayList<Score> scores = loadHS();
		boolean added = false;
		for (int i = 0; i < scores.size(); i++) {
			if (scores.get(i).score() < score.score()) {
				scores.add(i,score);
				added = true;
				break;
			}
		}
		if (!added) scores.add(scores.size(),score);
		while (scores.size() > 100)
			scores.remove(100);
		PrintWriter writer;
		try {
			writer = new PrintWriter("HighScores.txt", "UTF-8");
			for (Score s : scores) {
				writer.println(s.name());
				writer.println(s.score());
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static void displayHighScores() {
		JFrame frame = new JFrame("HIGHSCORES");
		int i = 100;
		String table = "<html><table border=\"1\"><tr><th></th><th width=\"100\"><big><big><big><big><big><big><big><big><big><big><big><big>Name</big></big></big></big></big></big></big></big></big></big></th><th width=\"300\"><big><big><big><big><big><big><big><big><big><big><big><big>Score</big></big></big></big></big></big></big></big></big></big></th></tr>";
		for (Score s : loadHS()) {
			table += "<tr><td><big><big><big><big><big><big><big><big><big><big><big><big>";
			table += i;
			table += "</big></big></big></big></big></big></big></big></big></big></h1></td><td><big><big><big><big><big><big><big><big><big><big><big><big>";
			table += "Level " + (s.score()/1000) + ": " + (s.score() % 1000);
			table += "</big></big></big></big></big></big></big></big></big></big></h1></td><td><big><big><big><big><big><big><big><big><big><big><big><big>";
			table += s.name();
			table += "</big></big></big></big></big></big></big></big></big></big></td></tr>";
			i--;
		}
		table += "</big></table></html>";
		
		JLabel label = new JLabel(table);
		frame.add(label);
		frame.setBounds(100,100,550,400);
        JScrollPane scrollFrame = new JScrollPane(label);
        frame.add(scrollFrame);
		frame.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		Score s = new Score("KT",100);
		Score s2 = new Score("KT4",50);
		Score s3 = new Score("KT3",60);
		Score s4 = new Score("KT2",70);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s);
		addHS(s2);
		addHS(s3);
		addHS(s4);
		displayHighScores();
	}
	
}