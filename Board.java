import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class Board extends JComponent {
	
	GraphNode[][] board;
	public boolean quit;
	int score;
	Player p, cpu;
	ArrayList<String> walls;
	public int mouseX, mouseY;
	boolean cpuPlaying;
	
	public Board(boolean cpuPlaying) {
		this.cpuPlaying = cpuPlaying;
		p = new Player("Player", 4, 8, 10);
		cpu = new Player("CPU", 4, 0, 10);
		score = 200;
		walls = new ArrayList<String>();
		board = new GraphNode[9][9];
		mouseX = -1;
		mouseY = -1;
		// create each graph node
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 9; y++) {
				board[x][y] = new GraphNode(x, y);
			}
		}
		// connect each graph node with the one to the side
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 9; y++) {
				GraphNode.connect(board[x][y], board[x+1][y]);
			}
		}
		// connect each graph node with the one vertically adjacent
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 8; y++) {
				GraphNode.connect(board[x][y], board[x][y+1]);
			}
		}
	}
	
	
	
	public void paintComponent(Graphics gc) {
		//blank background
		gc.setColor(Color.white);
		gc.fillRect(0, 0, 660, 780);
		gc.setColor(Color.black);
		
		// draw the hover ----------------------------------------------------
	    gc.setColor(new Color(245,245,155));
	    int col = mouseX / (60);
		int colrem = mouseX % (60);
		int row = mouseY / (60);
		int rowrem = mouseY % (60);
		
		if (colrem > 50 && rowrem > 80 ||
				mouseX < 0 || mouseX > 9*50 + 8*10 ||
				mouseY < 0 || mouseY > 9*50 + 8*10) {
		} else
		if (colrem > 50)
			gc.fillRect((col+1)*60+50,(row+2)*60,10,110);
		else if (rowrem > 50)
			gc.fillRect((col+1)*60,(row+2)*60+50,110,10);
		else 
			gc.fillRect((col+1)*60,(row+2)*60,50,50);
		// --------------------------------------------------------------------
		
		gc.setColor(Color.BLACK);
		for (int x = 1; x < 10; x++) {
			for (int y = 2; y < 11; y++) {
				gc.drawRect(x*60,y*60-1,50,50);
				gc.drawRect(x*60-1,y*60,50,50);
				gc.drawRect(x*60,y*60-1,50,50);
				gc.drawRect(x*60-1,y*60,50,50);
			}
		}
		gc.fillOval((cpu.x+1)*60+5,(cpu.y+2)*60+5,40,40);
		gc.setColor(Color.green);
		gc.fillOval((p.x+1)*60+5,(p.y+2)*60+5,40,40);
		
		// drawing the graph data structure representation
		//gc.setColor(Color.red);
		//for (int x = 1; x < 10; x++) {
		//	for (int y = 1; y < 10; y++) {
		//		gc.drawRect(x*50-5,y*50-5,10,10);
		//		if (board[x-1][y-1].north != null)
		//			gc.drawLine(x*50,y*50,x*50,(y-1)*50);
		//		if (board[x-1][y-1].east != null)
		//			gc.drawLine(x*50,y*50,(x+1)*50,y*50);
		//		if (board[x-1][y-1].south != null)
		//			gc.drawLine(x*50,y*50,x*50,(y+1)*50);
		//		if (board[x-1][y-1].west != null)
		//			gc.drawLine(x*50,y*50,(x-1)*50,y*50);
		//	}
	//	}
		gc.setColor(Color.black);
		for (String w : walls) {
			int x1 = Integer.parseInt(w.substring(0,1));
			int y1 = Integer.parseInt(w.substring(1,2));
			String d1 = w.substring(2,3);
			if (d1.equals("h")) {
				gc.fillRect((x1+1)*60,(y1+2)*60+50,110,10);
			}
			if (d1.equals("v")) {
				gc.fillRect((x1+1)*60+50,(y1+2)*60,10,110);
			}
		}
		
		
		
		// labels and buttons
		gc.setFont(new Font("TimesRoman", Font.PLAIN, 40)); 
		gc.drawString("Walls: "+cpu.wallsLeft, 60, 100);
		gc.drawString("Walls: "+p.wallsLeft, 60, 690);
		if (!cpuPlaying) {
			gc.setColor(new Color(240,240,240));
			gc.fillRect(470,60,130,50);
			gc.setColor(Color.black);
			gc.drawString("UNDO", 480, 100);
			gc.drawRect(470,60,130,50);
		}
		gc.setColor(new Color(240,240,240));
		gc.fillRect(470,8,130,50);
		gc.setColor(Color.black);
		gc.drawString("QUIT", 480, 48);
		gc.drawRect(470,8,130,50);
		gc.drawString("Player "+(score%2+1)+" is up", 230, 60);
		gc.drawLine(220,70,450,70);
		gc.drawLine(220,70,220,20);
		gc.drawLine(450,70,450,20);
		gc.drawString("SCORE: "+score, 380, 690);
		

	}
	
	
	public void addWall(Wall w, String str) {
		walls.add(str);
		GraphNode.disconnect(board[w.x1][w.y1],board[w.x2][w.y2]);
	}
	
	public void removeWall(Wall w, String str) {
		walls.remove(str);
		GraphNode.connect(board[w.x1][w.y1],board[w.x2][w.y2]);
	}
	
	public void disconnect(int x, int y, String dir) {
		if (dir.equals("N"))
			GraphNode.disconnect(board[x][y],board[x][y-1]);
		if (dir.equals("S"))
			GraphNode.disconnect(board[x][y],board[x][y+1]);
		if (dir.equals("E"))
			GraphNode.disconnect(board[x][y],board[x+1][y]);
		if (dir.equals("W"))
			GraphNode.disconnect(board[x][y],board[x-1][y]);
	}	
	public void connect(int x, int y, String dir) {
		if (dir.equals("N"))
			GraphNode.connect(board[x][y],board[x][y-1]);
		if (dir.equals("S"))
			GraphNode.connect(board[x][y],board[x][y+1]);
		if (dir.equals("E"))
			GraphNode.connect(board[x][y],board[x+1][y]);
		if (dir.equals("W"))
			GraphNode.connect(board[x][y],board[x-1][y]);
	}
	
	public void alienateNode(int x, int y) {
		if (y-1 >= 0)
			GraphNode.disconnect(board[x][y],board[x][y-1]);
		if (y+1 < 0)
			GraphNode.disconnect(board[x][y],board[x][y+1]);
		if (x+1 < 0)
			GraphNode.disconnect(board[x][y],board[x+1][y]);
		if (x-1 >= 0)
			GraphNode.disconnect(board[x][y],board[x-1][y]);
	}
	
	public void rejoinNode(int x, int y) {
		if (y-1 >= 0)
			GraphNode.connect(board[x][y],board[x][y-1]);
		if (y+1 < 0)
			GraphNode.connect(board[x][y],board[x][y+1]);
		if (x+1 < 0)
			GraphNode.connect(board[x][y],board[x+1][y]);
		if (x-1 >= 0)
			GraphNode.connect(board[x][y],board[x-1][y]);
	}
	
	
	public int distFrom(int x, int y, int goal, ArrayList<GraphNode> arr) {
		if (y == goal) return 0;
		//if (isLoop(arr, board[x][y])) return 1000000;
		int dist = 1000000;
		int d2;
		GraphNode gn = board[x][y];
		arr.add(gn);
		if (gn.north != null) {
			disconnect(x, y, "N");
			d2 = distFrom(x,y-1,goal, arr);
			connect(x, y, "N");
			if (d2 < dist)
				dist = d2 + 1;
		}
		if (gn.east != null) {
			disconnect(x, y, "E");
			d2 = distFrom(x+1,y,goal, arr);
			connect(x, y, "E");
			if (d2 < dist)
				dist = d2 + 1;
		}
		if (gn.west != null) {
			disconnect(x, y, "W");
			d2 = distFrom(x-1,y,goal, arr);
			connect(x, y, "W");
			if (d2 < dist)
				dist = d2 + 1;
		}
		if (gn.south != null) {
			disconnect(x, y, "S");
			d2 = distFrom(x,y+1,goal, arr);
			connect(x, y, "S");
			if (d2 < dist)
				dist = d2 + 1;
		}
		return dist;
	}
	

	public String toString() {
		String acc = "walls ";
		for (String str : walls)
			acc += str + " ";
		acc += "\n";
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 9; i++) {
				if (board[i][j].north != null)
					acc += "N";
				else acc += " ";
				if (board[i][j].south!= null)
					acc += "S";
				else acc += " ";
				if (board[i][j].east != null)
					acc += "E";
				else acc += " ";
				if (board[i][j].west != null)
					acc += "W ";
				else acc += " ";
			}
			acc += "\n";
		}
		return acc;
	}
	

	
	
	public Integer[][] distArray(int x, int y) {
		return distArray(x,y,new Integer[9][9], 0);
	}	
	public Integer[][] distArray(int x, int y, Integer[][] arr, int i) {
		if (arr[x][y] == null)
			arr[x][y] = i;
		else if (arr[x][y] <= i)
			return arr;
		if (board[x][y].north != null)
			arr = distArray(x, y - 1, arr, i+1);
		if (board[x][y].south != null)
			arr = distArray(x, y + 1, arr, i+1);
		if (board[x][y].east != null)
			arr = distArray(x + 1, y, arr, i+1);
		if (board[x][y].west != null)
			arr = distArray(x - 1, y, arr, i+1);
		return arr;
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame("test");
		f.setBounds(100,100,600,600);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Board board = new Board(false);
		//board.addWall(new Wall(1,0,2,0));
	//	board.addWall(new Wall(1,1,2,1));
	//	board.addWall(new Wall(1,1,1,2));
	//	board.addWall(new Wall(0,1,0,2));
	//	board.addWall(new Wall(4,8,4,7));
	//	board.addWall(new Wall(3,8,3,7));
	//	board.addWall(new Wall(4,5,3,5));
	//	board.addWall(new Wall(4,5,4,4));
	//	board.addWall(new Wall(5,5,5,4));
	//	board.addWall(new Wall(6,5,6,4));
	//	board.addWall(new Wall(7,5,7,4));
	//	//board.addWall(new Wall(8,5,8,4));
	//	board.addWall(new Wall(3,8,2,8));
	//	f.add(board);
		board.p.x = 8;
		board.p.y = 5;
		ArrayList<ArrayList<Point>> solutions =  PossibleSolutions.getSolutions(board, board.cpu, 8);
		System.out.println(Board.min(solutions));
		for (int in : board.getQuadValue())
			System.out.print(in + " ");
		System.out.println("\n--------------");
		System.out.println(DistanceTable.shortestRoute(board)[0]);
		System.out.println(DistanceTable.shortestRoute(board)[1]);
		board.paintImmediately(0,0,600,600);
	}
	
	
	
	
	
	public static int min(ArrayList<ArrayList<Point>> array) {
		if (array.size() == 0) return -1;
		if (array.size() == 1) return array.get(0).size() - 1;
		int i = array.get(0).size() - 1;
		array.remove(0);
		return Math.min(i, min(array));
	}
	
	// NORTH[NSEW] SOUTH[NSEW] EAST[NSEW] WEST[NSEW] for P, NSEW for CPU
	public int[] getQuadValue() {
		int[] vals = new int[8];
		Player tp = new Player("tester",p.x,p.y, 0);
		if (board[tp.x][tp.y].north != null) {
			tp.move("N");
			vals[0] = Board.min(PossibleSolutions.getSolutions(this, tp, 0));
			tp.move("S");
		}
		if (board[tp.x][tp.y].south != null) {
			tp.move("S");
			vals[1] = Board.min(PossibleSolutions.getSolutions(this, tp, 0));
			tp.move("N");
		}
		if (board[tp.x][tp.y].east != null) {
			tp.move("E");
			vals[2] = Board.min(PossibleSolutions.getSolutions(this, tp, 0));
			tp.move("W");
		}
		if (board[tp.x][tp.y].west != null) {
			tp.move("W");
			vals[3] = Board.min(PossibleSolutions.getSolutions(this, tp, 0));
			tp.move("E");
		}
		tp = new Player("tester",cpu.x,cpu.y, 0);
		if (board[tp.x][tp.y].north != null) {
			tp.move("N");
			vals[4] = Board.min(PossibleSolutions.getSolutions(this, tp, 8));
			tp.move("S");
		}
		if (board[tp.x][tp.y].south != null) {
			tp.move("S");
			vals[5] = Board.min(PossibleSolutions.getSolutions(this, tp, 8));
			tp.move("N");
		}
		if (board[tp.x][tp.y].east != null) {
			tp.move("E");
			vals[6] = Board.min(PossibleSolutions.getSolutions(this, tp, 8));
			tp.move("W");
		}
		if (board[tp.x][tp.y].west != null) {
			tp.move("W");
			vals[7] = Board.min(PossibleSolutions.getSolutions(this, tp, 8));
			tp.move("E");
		}
		return vals;
	}
	
	
	
	
	
	
}