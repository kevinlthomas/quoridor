public class GraphNode {
	
	
	int x, y; // node ID
	GraphNode north, south, east, west;
	
	public GraphNode(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public static void connect(GraphNode n1, GraphNode n2) {
		if (n1.x == n2.x && n1.y == n2.y - 1) {
			n1.south = n2;
			n2.north = n1;
		}
		if (n1.x == n2.x && n1.y == n2.y + 1) {
			n1.north = n2;
			n2.south = n1;
		}
		if (n1.x == n2.x + 1&& n1.y == n2.y) {
			n1.west = n2;
			n2.east = n1;
		}
		if (n1.x == n2.x - 1&& n1.y == n2.y) {
			n1.east = n2;
			n2.west = n1;
		}
	}
	
	public static void disconnect(GraphNode n1, GraphNode n2) {
		if (n1.x == n2.x && n1.y == n2.y - 1) {
			n1.south = null;
			n2.north = null;
		}
		if (n1.x == n2.x && n1.y == n2.y + 1) {
			n1.north = null;
			n2.south = null;
		}
		if (n1.x == n2.x + 1&& n1.y == n2.y) {
			n1.west = null;
			n2.east = null;
		}
		if (n1.x == n2.x - 1&& n1.y == n2.y) {
			n1.east = null;
			n2.west = null;
		}
	}
}