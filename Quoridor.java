import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;

public class Quoridor  {

	String page;
	public int level;

	public Quoridor() {
		page = "home";
		level = -1;
	}



	public static void main(String[] args) {
		final Quoridor q = new Quoridor();
		JFrame fr = new JFrame("Quoridor");
		fr.setBounds(100,100,800,800);
		fr.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//f.setUndecorated(true);
		// --------------------------------------------------home screen
        JPanel homeV = new JPanel();
		homeV.setLayout(new BoxLayout(homeV, BoxLayout.Y_AXIS));
        JLabel label = new JLabel("QUORIDOR");
        homeV.add(label);
		label.setFont(new Font("TimesRoman", Font.PLAIN, 80));


		JPanel instructions = new JPanel();
		TitledBorder ititle = new TitledBorder("Instructions");
		ititle.setTitleFont(new Font("TimesRoman", Font.PLAIN, 50));
		JLabel ibox = new JLabel("<html><h1><center>Purpose Of The Game</center></h1>" +
				"<p>    When the game starts, each player has 10 fences in their possesion and their pawn in the center of his baseline</p>" +
				"<h1><center>Rules Of The Game</center></h1>" +
				"<p>Each player in his turn chooses to move his pawn or to add one of his fences to the board. When the player has run out of fences the player must move his pawn.<br>"+
				"<h2>Pawn Moves</h2>The pawns are moved one square at a time, horizontally or vertically, forwards or backwards. The pawns must go around all fences.<br>"+
				"<h2>Positioning Of The Fences</h2>The fences must be placed between 2 sets of 2 squares. The fences can be used to facilitate the player's progress or to impede that of the opponent."+
				"However, an access to the goal line must always be left open.<br>"+
				"<h2>Face to Face</h2>When two pawns face each other on neighbouring spaces which are not seperated by a fence, the player whose turn it is can jump the opponent's pawn (and place himself behind him), this advancing an extra square. If there is a fence behind the said pawn, the player can place his pawn to the left or the right of the other pawn.</p>"+
				"<h1><center>End of the game</center></h1>"+
				"<p>The first player who reaches one of the 9  square opposite his baseline is the winner</p>"+
				"</big></html>"
);

        instructions.setBorder(ititle);
        instructions.add(ibox);
        ibox.setPreferredSize(new Dimension(700,500));
        homeV.add(instructions);





        JPanel buttons = new JPanel();
        JButton p2 = new JButton("2 Player Game");
        JButton cpu1 = new JButton("Play CPU - Level 1");
        JButton cpu2 = new JButton("Play CPU - Level 2");
        JButton hs = new JButton("VIEW HIGHSCORES");
        p2.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent evt) {
    		  q.level = 0;
      	  }
      	});
        buttons.add(p2);
        cpu1.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent evt) {
    		  q.level = 1;
      	  }
      	});
        buttons.add(cpu1);
        cpu2.addActionListener(new ActionListener() {
        	  public void actionPerformed(ActionEvent evt) {
        		  q.level = 2;
        	  }
        	});
        buttons.add(cpu2);
        hs.addActionListener(new ActionListener() {
      	  public void actionPerformed(ActionEvent evt) {
      		  HighScores.displayHighScores();
      	  }
      	});
        buttons.add(hs);
        homeV.add(buttons);



        JPanel home = new JPanel();
		home.setLayout(new BoxLayout(home, BoxLayout.X_AXIS));
		JPanel left = new JPanel();
		JPanel right = new JPanel();
		home.add(left);
		home.add(homeV);
		home.add(right);
        fr.add(home);
		fr.setVisible(true);
		while (true) {
			System.out.print("");
		if (q.level >= 0 ) {
			fr.setVisible(false);
			q.run(q.level);
			q.level = -1;
			fr.setVisible(true);
		}
		}
	}

	public void run(int level) {
		if (level == 0) {
			  PlayGame game = new PlayGame(false, level);
			  getName4HW(game.runGame()+level*1000);
			  return;

		}
		  PlayGame game = new PlayGame(true, level);
		  getName4HW(game.runGame()+level*1000);
		  return;
	}



	private void getName4HW(int runGame) {
		System.out.println(runGame);
		if (runGame%1000 == 0) return;
		String s = (String)JOptionPane.showInputDialog(
		                    null,
		                    "Please enter your name for the high score chart!",
		                    "HIGHSCORE",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null,
		                    null,
		                    "");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			HighScores.addHS(new Score(s, runGame));
		    return;
		}

		//If you're here, the return value was null/empty.

	}

}
