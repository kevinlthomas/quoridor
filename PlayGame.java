import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PlayGame {
	

		Integer mouseX, mouseY;
	 Board board;
	 JFrame f;
	 boolean isPlayer;
	 ArrayList<String> moves;
	 boolean cpuGame;
	 int level;
	 
	public PlayGame(Boolean cpu, int level) {
		cpuGame = cpu;
		board = new Board(cpu);
		f = new JFrame("Quoridor - 1 Player");
		  f.addMouseListener(new MouseAdapter() {
		     @Override
		     public void mousePressed(MouseEvent e) {
		    	    mouseX = e.getX()-70;
		    	    mouseY = e.getY()-164;
		     }
		  });

		  f.addMouseMotionListener(new MouseAdapter() {
		     @Override
		     public void mouseMoved(MouseEvent e) {
		    	    board.mouseX = e.getX()-70;
		    	    board.mouseY = e.getY()-164;
		     }
		  });
		f.setBounds(100,100,660,780);
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		f.add(board);
		f.setVisible(true);
		isPlayer = true;
		moves = new ArrayList<String>();
		this.level = level;
	}
	
	public int runGame() {
		board.paintImmediately(0,0,660,780);
		Scanner sc = new Scanner(System.in);
		while (true) {
			if (board.quit) {
				f.setVisible(false);
				return 0;
			}
			String str = this.getCommand();// sc.nextLine();
			exacute(str);
			board.paintImmediately(0,0,660,780);
			if (isGameOver()) {
				if (!isPlayer) {
					JOptionPane.showMessageDialog(f, "Player 1 Wins!");
					if (cpuGame) {
						f.setVisible(false);
						return board.score;
					}
					f.setVisible(false);
					return 0;
				}
				else {
					if (cpuGame)
						JOptionPane.showMessageDialog(f, "Computer Wins!");
					else 
						JOptionPane.showMessageDialog(f, "Player 2 Wins!");
					f.setVisible(false);
					return 0;
				}
			}
			// -------------if AI is playin, he's gonna move -----------
			if (cpuGame && !isPlayer) {
				if (board.quit) {
					f.setVisible(false);
					return 0;
				}
				String cpustr = bestMove();//AI GEERATED COMMAND
				exacute(cpustr);
				board.paintImmediately(0,0,600,600);
				if (isGameOver()) {
					if (!isPlayer) {
						JOptionPane.showMessageDialog(f, "Player 1 Wins!");
						if (cpuGame) {
							f.setVisible(false);
							return board.score;
						}
					}
					else {
						if (cpuGame)
							JOptionPane.showMessageDialog(f, "Computer Wins!");
						else 
							JOptionPane.showMessageDialog(f, "Player 2 Wins!");
						f.setVisible(false);
						return 0;
					}
				}
			}
		}
	}


	public void exacute(String str) {
		if (str.equals("quit")) {
			board.quit = true;
			return;
		}
		if (str.equals("undo")) {
			undo();
			return;
		}
		if (str.length() == 2) {
			if (!isValidMove(str)) {
				JOptionPane.showMessageDialog(f, "Sorry, cannot move there");
				return;
			}
			if (isPlayer) {
				board.p.x = Integer.parseInt(str.substring(0,1));
				board.p.y = Integer.parseInt(str.substring(1,2));
			}
			else {
				board.cpu.x = Integer.parseInt(str.substring(0,1));
				board.cpu.y = Integer.parseInt(str.substring(1,2));
			}
		}
		if (str.length() == 3) {
			if (!isValidWall(str)) {
				JOptionPane.showMessageDialog(f, "Sorry, invalid spot for a wall");
				return;
			}
			if (isPlayer) board.p.wallsLeft--;
			else board.cpu.wallsLeft--;
			if (isPlayer && board.p.wallsLeft < 0) {
				JOptionPane.showMessageDialog(f, "Sorry, no more walls available");
				board.p.wallsLeft = 0;
				return;
			}
			if (!isPlayer && board.cpu.wallsLeft < 0) {
				JOptionPane.showMessageDialog(f, "Sorry, no more walls available");
				board.cpu.wallsLeft = 0;
				return;
			}
			if (str.charAt(2) == 'h') {
				int x = Integer.parseInt(str.substring(0,1));
				int y = Integer.parseInt(str.substring(1,2));
				board.addWall(new Wall(x, y, x, y+1), str);
				board.addWall(new Wall(x+1, y, x+1, y+1), str);
			}
			if (str.charAt(2) == 'v') {
				int x = Integer.parseInt(str.substring(0,1));
				int y = Integer.parseInt(str.substring(1,2));
				board.addWall(new Wall(x, y, x+1, y), str);
				board.addWall(new Wall(x, y+1, x+1, y+1), str);
			}
		}

		isPlayer = !isPlayer;
		moves.add(0,str);
		board.score--;
		if (!isValidBoard()) {
			undo();
			JOptionPane.showMessageDialog(f, "Sorry, invalid move, must leave at least one path");
			return;
		}
	}
	


	public void undo() {
		if (moves.size() == 0) return;
		board.score++;
		String str = moves.get(0);
		isPlayer = !isPlayer;
		if (str.length() == 2) {
			if (isPlayer) {
				String str2 = "48";
				for (int i = 2; i < moves.size(); i += 2) {
					if (moves.get(i).length() == 2) {
						str2 = moves.get(i);
						break;
					}
				}
				board.p.x = Integer.parseInt(str2.substring(0,1));
				board.p.y = Integer.parseInt(str2.substring(1,2));
			}
			else {
				String str2 = "40";
				for (int i = 2; i < moves.size(); i += 2) {
					if (moves.get(i).length() == 2) {
						str2 = moves.get(i);
						break;
					}
				}
				board.cpu.x = Integer.parseInt(str2.substring(0,1));
				board.cpu.y = Integer.parseInt(str2.substring(1,2));
			}
		}
		if (str.length() == 3) {
			if (isPlayer) board.p.wallsLeft++;
			else board.cpu.wallsLeft++;
			if (str.charAt(2) == 'h') {
				int x = Integer.parseInt(str.substring(0,1));
				int y = Integer.parseInt(str.substring(1,2));
				board.removeWall(new Wall(x, y, x, y+1), str);
				board.removeWall(new Wall(x+1, y, x+1, y+1), str);
			}
			if (str.charAt(2) == 'v') {
				int x = Integer.parseInt(str.substring(0,1));
				int y = Integer.parseInt(str.substring(1,2));
				board.removeWall(new Wall(x, y, x+1, y), str);
				board.removeWall(new Wall(x, y+1, x+1, y+1), str);
			}
		}
		moves.remove(0);
	}
	
	// for a game with tile width w1 and wall width w2;
	public static String coordsToCommand(int x, int y, int w1, int w2) {
		if (x > 410 && x < 540 && y > -60 && y < -10) {
			return "undo";
		}
		if (x > 410 && x < 540 && y > -112 && y < -62) {
			return "quit";
		}
		int col = x / (w1+w2);
		int colrem = x % (w1+w2);
		int row = y / (w1+w2);
		int rowrem = y % (w1+w2);
		
		if (colrem > w1 && rowrem > 80 ||
				x < 0 || x > 9*w1 + 8*w2 ||
				y < 0 || y > 9*w1 + 8*w2) {
			return "";
		}
		if (colrem > w1)
			return col+""+row+"v";
		if (rowrem > w1)
			return col+""+row+"h";
		return col+""+row;
	}
	
	public String getCommand() {
		while (mouseX == null || mouseY == null) {
			board.paintImmediately(0,0,660,780);
		}
		String str = coordsToCommand(mouseX, mouseY, 50, 10);
		mouseX = null;
		mouseY = null;
		if (str.equals("")) 
			return getCommand();
		return str;
	}
	
	public static void main(String[] args) {
		PlayGame game = new PlayGame(true, 2);
		game.runGame();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public boolean isValidBoard() {
		int[] dists = DistanceTable.shortestRoute(board);
		if (dists[0] + dists[1] > 999999)
			return false;
		return true;
		
	}
	
	public boolean isValidMove(String str) {
		int x1 = Integer.parseInt(str.substring(0,1));
		int y1 = Integer.parseInt(str.substring(1,2));
		int x0, y0, x2, y2;
		ArrayList<String> viable = new ArrayList<String>();
		if (isPlayer) {
			x0 = board.p.x;
			y0 = board.p.y;
			x2 = board.cpu.x;
			y2 = board.cpu.y;
		} else {
			x0 = board.cpu.x;
			y0 = board.cpu.y;
			x2 = board.p.x;
			y2 = board.p.y;
		}
		if ((Math.abs(x1-x0)+Math.abs(y1-y0)) > 2)
			return false;
		if (board.board[x0][y0].north != null)
			viable.add(x0 + "" + (y0 - 1));
		if (board.board[x0][y0].south != null)
			viable.add(x0 + "" + (y0 + 1));
		if (board.board[x0][y0].east != null)
			viable.add((x0 + 1) + "" + y0);
		if (board.board[x0][y0].west != null)
			viable.add((x0 - 1) + "" + y0);
		if (viable.contains(x2 +"" + y2)) {
			viable.remove(x2 +"" + y2);
			if (board.board[x2][y2].north != null)
				viable.add(x2 + "" + (y2 - 1));
			if (board.board[x2][y2].south != null)
				viable.add(x2 + "" + (y2 + 1));
			if (board.board[x2][y2].east != null)
				viable.add((x2 + 1) + "" + y2);
			if (board.board[x2][y2].west != null)
				viable.add((x2 - 1) + "" + y2);
		}
		viable.remove(x0 +"" + y0);
		return viable.contains(str);
	}
	
	public boolean isValidWall(String command) {
		int x1 = Integer.parseInt(command.substring(0,1));
		int y1 = Integer.parseInt(command.substring(1,2));
		String d1 = command.substring(2,3);
		if (x1 < 0 || x1 > 7 || y1 < 0 || y1 > 7)
			return false;
		for (String wall : board.walls) {
			int x2 = Integer.parseInt(wall.substring(0,1));
			int y2 = Integer.parseInt(wall.substring(1,2));
			String d2 = wall.substring(2,3);
			if (x1 == x2 && y1 == y2)
				return false;
			if (d1.equals(d2)) {
				if (d1.equals("h")) 
					if ((x1 + 1 == x2 || x1 - 1 == x2) && y1 == y2)
						return false;
				if (d1.equals("v"))
					if ((y1 + 1 == y2 || y1 - 1 == y2) && x1 == x2)
						return false;
			}
			
		}
		return true;
	}
	
	public boolean isGameOver() {
		return (board.p.y == 0 || board.cpu.y == 8);
	}
	
	public boolean isValidCommand(String c) {
		if (c.length() == 2) {
			return isValidMove(c);
		}
		if (c.length() == 3) {
			if (!isValidWall(c))
				return false;
			// add the wall manually and check if it seals off
			if (c.charAt(2) == 'h') {
				int x = Integer.parseInt(c.substring(0,1));
				int y = Integer.parseInt(c.substring(1,2));
				board.addWall(new Wall(x, y, x, y+1), c);
				board.addWall(new Wall(x+1, y, x+1, y+1), c);
			}
			if (c.charAt(2) == 'v') {
				int x = Integer.parseInt(c.substring(0,1));
				int y = Integer.parseInt(c.substring(1,2));
				board.addWall(new Wall(x, y, x+1, y), c);
				board.addWall(new Wall(x, y+1, x+1, y+1), c);
			}
			if (isPlayer) board.p.wallsLeft--;
			else board.cpu.wallsLeft--;
			isPlayer = !isPlayer;
			moves.add(0,c);
			board.score--;
			boolean result = isValidBoard();
			undo();
			return result;
			
		}
		return false;
	}
	
	
	
	
	//   -------------- AI PORTION ---------------------------------------
	
	public String bestMove() {
		return maxScoreMove(level);
	}
	
	// board is scored by, higher is better
	public int scoreBoardCPU() {
		if (board.p.y == 0) return -1000;
		if (board.cpu.y == 8) return 1000;
		int[] dists = DistanceTable.shortestRoute(board);
		return (board.cpu.wallsLeft*2 + dists[0]) - (board.p.wallsLeft*2 + dists[1]);
	}
	// board is scored by, higher is better
	public int scoreBoardP1() {
		int[] dists = DistanceTable.shortestRoute(board);
		return (board.p.wallsLeft*1 + dists[1]) - (board.cpu.wallsLeft*1 + dists[0]);
	}
	
	
	// MINIMAX ALGORITHM --------------------------------------------------------------------------Minimax
	public String maxScoreMove(int depth) {
		//System.out.println(board);
		//System.out.println(new DistanceTable(board.cpu.x, board.cpu.y, board));
		String str = "xx";
		int maxScore = -1000000;
		int score;
		for (int i = board.cpu.x - 2; i <= board.cpu.x + 2; i++) {
			for (int j = board.cpu.y - 2; j <= board.cpu.y + 2; j++) {
				if (i >= 0 && i < 9 && j >= 0 && j < 9)
				if (isValidCommand(i+""+j)) {
					//System.out.print(i+""+j+": ");
					exacute(i+""+j);
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth-1);
					//System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j;
					}
					exacute("undo");
				}
			}
		}
		if (board.cpu.wallsLeft > 0)
		for (int i = 0; i < 8; i++ ) {
			for (int j = 0; j < 8; j++) {
				if (isValidCommand(i+""+j+"h")) {
					//System.out.print(i+""+j+"h: ");
					exacute(i+""+j+"h");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth-1);
					//System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j+"h";
					}
					exacute("undo");
				}

				if (isValidCommand(i+""+j+"v")) {
					//System.out.print(i+""+j+"v: ");
					exacute(i+""+j+"v");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth-1);
					//System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j+"v";
					}
					exacute("undo");
				}
			}
		}
		//System.out.println(new DistanceTable(board.cpu.x, board.cpu.y, board));
		//System.out.println(board);
		//System.out.println(maxScore - scoreBoardCPU());
		return str;
	}

	private int minScore(int depth) {
		//System.out.println("Min----------"+depth);
		String str = "xx";
		int minScore = 1000000;
		int score;
		for (int i = board.p.x - 2; i <= board.p.x + 2; i++) {
			for (int j = board.p.y - 2; j <= board.p.y + 2; j++) {
				if (i >= 0 && i < 9 && j >= 0 && j < 9)
				if (isValidCommand(i+""+j)) {
				//	System.out.print(i+""+j+": ");
					exacute(i+""+j);
					if (depth == 1)
						score = scoreBoardCPU();
					else score = maxScore(depth-1);
				//	System.out.println(score);
					if (score < minScore) {
						minScore = score;
						str = i+""+j;
					}
					exacute("undo");
				}
			}
		}
		if (board.p.wallsLeft > 0)
		for (int i = 0; i < 8; i++ ) {
			for (int j = 0; j < 8; j++) {
				if (isValidCommand(i+""+j+"h")) {
				//	System.out.print(i+""+j+"h: ");
					exacute(i+""+j+"h");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = maxScore(depth-1);
				//	System.out.println(score);
					if (score < minScore) {
						minScore = score;
						str = i+""+j+"h";
					}
					exacute("undo");
				}

				if (isValidCommand(i+""+j+"v")) {
				//	System.out.print(i+""+j+"v: ");
					exacute(i+""+j+"v");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth-1);
				//	System.out.println(score);
					if (score < minScore) {
						minScore = score;
						str = i+""+j+"v";
					}
					exacute("undo");
				}
			}
		}
		//System.out.println(new DistanceTable(board.cpu.x, board.cpu.y, board));
		//System.out.println(board);
		//System.out.println(maxScore - scoreBoardCPU());
		return minScore;
	}
	
	public int maxScore(int depth) {
		//System.out.println("Max------------"+depth);
		//System.out.println(board);
		//System.out.println(new DistanceTable(board.cpu.x, board.cpu.y, board));
		String str = "xx";
		int maxScore = -1000000;
		int score;
		for (int i = board.cpu.x - 2; i <= board.cpu.x + 2; i++) {
			for (int j = board.cpu.y - 2; j <= board.cpu.y + 2; j++) {
				if (i >= 0 && i < 9 && j >= 0 && j < 9)
				if (isValidCommand(i+""+j)) {
				//	System.out.print(i+""+j+": ");
					exacute(i+""+j);
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth);
				//	System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j;
					}
					exacute("undo");
				}
			}
		}
		if (board.cpu.wallsLeft > 0)
		for (int i = 0; i < 8; i++ ) {
			for (int j = 0; j < 8; j++) {
				if (isValidCommand(i+""+j+"h")) {
				//	System.out.print(i+""+j+"h: ");
					exacute(i+""+j+"h");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth);
					//System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j+"h";
					}
					exacute("undo");
				}

				if (isValidCommand(i+""+j+"v")) {
				//	System.out.print(i+""+j+"v: ");
					exacute(i+""+j+"v");
					if (depth == 1)
						score = scoreBoardCPU();
					else score = minScore(depth);
					//System.out.println(score);
					if (score > maxScore) {
						maxScore = score;
						str = i+""+j+"v";
					}
					exacute("undo");
				}
			}
		}
		//System.out.println(new DistanceTable(board.cpu.x, board.cpu.y, board));
		//System.out.println(board);
		//System.out.println(maxScore - scoreBoardCPU());
		return maxScore;
	}
	
	// ALPHA BETA PRUNING ----------------------------------------------------------------------------alpha beta
	
	
	
}