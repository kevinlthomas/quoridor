import java.awt.Point;
import java.util.ArrayList;

public class PossibleSolutions {
	
	public static ArrayList<ArrayList<Point>> getSolutions(Board b, Player p, int goal) {
		DirectedGraph dg = new DirectedGraph(p.x, p.y, b);
		//dg.setInitDist();
		//dg.updateDist(dg.dg[p.x][p.y]);
		//System.out.println("SHORTEST PATH: " + dg.dg[p.x][p.y].dist);
		ArrayList<ArrayList<Point>> allsolutions = dg.converToSequences(dg.dg[p.x][p.y]);
		ArrayList<ArrayList<Point>> solutions = new ArrayList<ArrayList<Point>>();
		for (ArrayList<Point> sol : allsolutions) {
			if (sol.get(sol.size()-1).y == goal) {
				ArrayList<Point> newsol = new ArrayList<Point>();
				for (int n = 0; n < sol.size(); n++) {
					newsol.add(sol.get(n));
					if (sol.get(n).y == goal) break;
				}
				solutions.add(newsol);
			}
		}
		return solutions;
		
	}
	
	public static ArrayList<String> getStringSolutions(Board b, Player p, int goal) {
		DirectedGraph dg = new DirectedGraph(p.x, p.y, b);
		dg.setInitDist();
		dg.updateDist(dg.dg[p.x][p.y]);
		ArrayList<String> allsolutions = dg.stringify(dg.dg[p.x][p.y]);
		ArrayList<String> solutions = new ArrayList<String>();
		for (String string : allsolutions) {
			if (string.charAt(string.length()-2) == '0')
				solutions.add(string);
		}
		return solutions;
		
	}
	
	
}