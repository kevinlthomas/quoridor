public class Player {
	int wallsLeft;
	String name;
	int x, y;
	
	public Player(String n, int x, int y, int w) {
		name = n;
		this.x = x;
		this.y = y;
		wallsLeft = w;
	}
	
	public void move(String str) {
		if (str.equals("N"))
			y -= 1;
		if (str.equals("S"))
			y += 1;
		if (str.equals("E"))
			x += 1;
		if (str.equals("W"))
			x -= 1;
		
	}
}