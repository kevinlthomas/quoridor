	public class Score {
		
		public int score;
		public String name;
		
		public Score(String s, int x) {
			score = x;
			name = s;
		}
		
		public int score() {
			return score;
		}
		
		public String name() {
			return name;
		}
		
		public String toString() {
			return name + ": " + score;
		}
	}
	